﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class LogWriter
    {
        public static readonly log4net.ILog loginfo = log4net.LogManager.GetLogger("loginfo");
        public static readonly log4net.ILog logerror = log4net.LogManager.GetLogger("logerror");
        public static readonly log4net.ILog logdebug = log4net.LogManager.GetLogger("logdebug");
        public static readonly log4net.ILog logother = log4net.LogManager.GetLogger("logother");

        public static void SetConfig()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        public static void SetConfig(FileInfo configFile)
        {
            log4net.Config.XmlConfigurator.Configure(configFile);
        }
        /// <summary>
        /// 输出错误日志
        /// </summary>
        /// <param name="exception"></param>
        public static void WriteError(Exception exception, string title)
        {
            if (logerror.IsErrorEnabled)
            {
                logerror.Error(title, exception);
            }
        }

        public static void WriteError(string message, string title)
        {
            if (logerror.IsErrorEnabled)
            {
                logerror.Error(title, new Exception(message));
            }
        }

        public static void WriteError(Exception exception, string title, string message)
        {
            if (logerror.IsErrorEnabled)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(title);
                sb.Append(message);
                logerror.Error(sb.ToString(), exception);
            }
        }

        public static void WriteError(Exception exception, string title, object obj)
        {
            if (logerror.IsErrorEnabled)
            {
                string message = "";
                try
                {
                    message = JsonHelper.Serialize(obj);
                }
                catch (Exception)
                {
                }
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(title);
                sb.Append(message);
                logerror.Error(sb.ToString(), exception);
            }
        }

        public static void WriteError(Exception exception, MethodBase method, params object[] inputs)
        {
            ParameterInfo[] _params = method.GetParameters();
            int i = 0;
            StringBuilder sb = new StringBuilder();
            foreach (ParameterInfo param in _params)
            {
                string p_name = param.Name;
                object p_value = null;
                if (inputs != null && inputs.Length > i)
                {
                    p_value = inputs[i];
                }
                if (p_value == null)
                {
                    sb.AppendLine(string.Format("参数[{0}] {1}={2} ", i, p_name, "null"));
                    continue;
                }

                Type p_type = p_value.GetType();
                sb.AppendLine(string.Format("参数[{0}] 类型：{1} ", i, p_type.ToString()));
                try
                {
                    sb.AppendLine(string.Format("参数[{0}] 数据：{1}={2} ", i, p_name, JsonHelper.Serialize(p_value)));
                }
                catch (Exception)
                {
                    sb.AppendLine(string.Format("参数[{0}] 数据：{1}={2} ", i, p_name, ""));
                }
                i++;
            }

            string title = method.DeclaringType.FullName + "." + method.Name;
            LogWriter.WriteError(exception, title, sb.ToString());
        }

        /// <summary>
        /// 输出日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="title"></param>
        public static void WriteInfo(string message, string title)
        {
            if (loginfo.IsInfoEnabled)
            {
                loginfo.Info(title, new Exception(message));
            }
        }

        /// <summary>
        /// 输出调试日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="title"></param>
        public static void WriteDebug(string message, string title)
        {
            if (logdebug.IsDebugEnabled)
            {
                logdebug.Info(title, new Exception(message));
            }
        }

        /// <summary>
        /// 输出其它日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="title"></param>
        public static void WriteOther(string message, string title)
        {
            if (logother.IsInfoEnabled)
            {
                logother.Info(title, new Exception(message));
            }
        }
    }
}
