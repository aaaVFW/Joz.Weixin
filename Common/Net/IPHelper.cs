﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Common.Net
{
    public class IPHelper
    {
        /// <summary>
        /// 获取客户端IP与代理IP
        /// </summary>
        /// <param name="listProxy">代理IP列表</param>
        /// <returns>客户端IP</returns>
        public static string GetClientIP()
        {
            string listProxy = "";
            return GetClientIP(out listProxy);
        }
        /// <summary>
        /// 获取客户端IP与代理IP
        /// </summary>
        /// <param name="listProxy">代理IP列表</param>
        /// <returns>客户端IP</returns>
        public static string GetClientIP(out string listProxy)
        {
            return GetClientIP(HttpContext.Current, out listProxy);
        }
        public static string GetClientIP(HttpContext context, out string listProxy)
        {
            string hostIP = context.Request.UserHostAddress;   //代理IP地址（如果不通过代理则为客户IP地址）
            string proxyAddr = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];  //包含代理的IP地址列表
            string remoteAddr = context.Request.ServerVariables["REMOTE_ADDR"];  //客户IP地址

            listProxy = "";
            if (proxyAddr != null && proxyAddr != String.Empty)
            {
                listProxy = proxyAddr + "," + ((remoteAddr != null) ? remoteAddr : "") + "," + hostIP;
            }
            #region  这里顺序需要再考虑
            string result = hostIP;
            if (result == null || result == String.Empty)
            {
                result = remoteAddr;
            }
            if (result == null || result == String.Empty)
            {
                result = proxyAddr;
            }
            #endregion
            return result;
        }
        
        public static bool QuerAreaFromSINA(string ip, ref string province, ref string city)
        {
            //新浪的IP地址查询接口：http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js（还有json和xml格式）
            //新浪多地域测试方法：http://int.dpool.sina.com.cn/ipl ... mp;ip=12.130.132.30

            //http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js&ip=202.115.148.87
            //var remote_ip_info = {"ret":1,"start":"202.115.144.0","end":"202.115.159.255","country":"\u4e2d\u56fd","province":"\u56db\u5ddd","city":"\u6210\u90fd","district":"","isp":"\u6559\u80b2\u7f51","type":"\u5b66\u6821","desc":"\u897f\u534e\u5927\u5b66\u6559\u80b2\u7f51"};
            //var remote_ip_info = {"ret":1,"start":"27.47.0.0","end":"27.47.63.255","country":"\u4e2d\u56fd","province":"\u5e7f\u4e1c","city":"\u5e7f\u5dde","district":"","isp":"\u8054\u901a","type":"","desc":""};

            string v = HttpRequestHelper.getContentUTF8("http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js&ip=" + ip);


            return false;
        }
        public static bool QuerAreaFromPCOnline(string ip, ref string province, ref string city)
        {
            //http://whois.pconline.com.cn/jsFunction.jsp?callback=jsShow&ip=27.47.17.98
            //http://whois.pconline.com.cn/ipJson.jsp?level=2&ip=27.47.17.98
            //if(window.IPCallBack) {IPCallBack({"ip":"202.115.148.89","pro":"四川省","city":"成都市","region":"","addr":"四川省成都市 教育网","regionNames":""});}

            string v = HttpRequestHelper.getContentUTF8("http://whois.pconline.com.cn/ipJson.jsp?ip=" + ip);

            return false;
        }
        public static bool QuerAreaFromSOHU(string ip, ref string province, ref string city)
        {
            //搜狐IP地址查询接口（默认GBK）：http://pv.sohu.com/cityjson
            //搜狐IP地址查询接口（可设置编码）：http://pv.sohu.com/cityjson?ie=utf-8
            //搜狐另外的IP地址查询接口：http://txt.go.sohu.com/ip/soip
            string v = HttpRequestHelper.getContentUTF8("http://whois.pconline.com.cn/ipJson.jsp?ip=" + ip);
            return false;
        }
        public static bool QuerAreaFrom126(string ip, ref string province, ref string city)
        {
            //http://ip.ws.126.net/ipquery?ip=202.115.148.89
            //var lo="四川省", lc="成都市"; var localAddress={city:"成都市", province:"四川省"}

            string v = HttpRequestHelper.getContentUTF8("http://whois.pconline.com.cn/ipJson.jsp?ip=" + ip);

            return false;
        }
    }
}
