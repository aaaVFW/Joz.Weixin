﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Common.Net
{
    public class Config
    {
        /// <summary>
        /// Master库连接字符串
        /// </summary>
        public static string MasterConnectionString
        {
            get
            {
                string result = "";
                object obj = System.Configuration.ConfigurationManager.ConnectionStrings["master_mssql"];
                if (obj != null)
                {
                    result = obj.ToString();
                }

                return result;
            }
        }

        public static string CorpConnectionString
        {
            get
            {
                string result = "";
                object obj = System.Configuration.ConfigurationManager.ConnectionStrings["corp_mssql"];
                if (obj != null)
                {
                    result = obj.ToString();
                }

                return result;
            }
        }

        public static string CorpID
        {
            get
            {
                string result = "";
                object obj = System.Configuration.ConfigurationManager.AppSettings["CorpID"];
                if (obj != null)
                {
                    result = obj.ToString();
                }

                return result;
            }
        }

        public static int ModelCache
        {
            get
            {
                int result = 0;
                object obj = System.Configuration.ConfigurationManager.AppSettings["ModelCache"];
                if (obj != null)
                {
                    int.TryParse(obj.ToString(), out result);
                }
                return result;
            }
        }

        private static bool _IsCookies = false;
        public static bool IsCookies
        {
            get
            {
                object obj = System.Configuration.ConfigurationManager.AppSettings["IsCookies"];
                if (obj != null)
                {
                    bool.TryParse(obj.ToString(), out _IsCookies);
                }

                return _IsCookies;
            }
        }

        private static bool _CheckLoginAuthCode = false;
        public static bool CheckLoginAuthCode
        {
            get
            {
                object obj = System.Configuration.ConfigurationManager.AppSettings["CheckLoginAuthCode"];
                if (obj != null)
                {
                    bool.TryParse(obj.ToString(), out _CheckLoginAuthCode);
                }

                return _CheckLoginAuthCode;
            }
        }

        private static bool _Release = true;
        public static bool Release
        {
            get
            {
                object obj = System.Configuration.ConfigurationManager.AppSettings["Release"];
                if (obj != null)
                {
                    bool.TryParse(obj.ToString(), out _Release);
                }

                return _Release;
            }
        }

        private static int _DefaultFileSize = 10;
        /// <summary>
        /// 默认上传下载文件大小 10M
        /// </summary>
        public static int DefaultFileSize
        {
            get
            {
                object obj = System.Configuration.ConfigurationManager.AppSettings["DefaultFileSize"];
                if (obj != null)
                {
                    int.TryParse(obj.ToString(), out _DefaultFileSize);
                }
                return _DefaultFileSize;
            }
        }

        private static int _PageFrame = 0;
        /// <summary>
        /// 页面框架类型 0:使用Ajax加载页面，1:使用Iframe加载页面
        /// </summary>
        public static int PageFrame
        {
            get
            {
                object obj = System.Configuration.ConfigurationManager.AppSettings["PageFrame"];
                if (obj != null)
                {
                    int.TryParse(obj.ToString(), out _PageFrame);
                }
                return _PageFrame;
            }
        }

        private static int _CookieExpires = -1;
        /// <summary>
        /// 默认 -1 使用默认Cookies过期时间，关闭IE了，COOKIE就失效了
        /// </summary>
        public static int CookieExpires
        {
            get
            {
                object obj = System.Configuration.ConfigurationManager.AppSettings["CookieExpires"];
                if (obj != null)
                {
                    int.TryParse(obj.ToString(), out _CookieExpires);
                }
                return _CookieExpires;
            }
        }

        /// <summary>
        /// 文件服务上传路径
        /// </summary>
        public static string FSUploadPath
        {
            get
            {
                string result = null;
                try
                {
                    object obj = System.Configuration.ConfigurationManager.AppSettings["FSUploadPath"];
                    if (obj != null)
                    {
                        result = obj.ToString();
                    }
                    if (string.IsNullOrEmpty(result))
                    {
                        if (HttpContext.Current != null)
                        {
                            result = HttpContext.Current.Server.MapPath("~/UploadFiles");
                        }
                        else
                        {
                            result = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "UploadFiles");
                        }
                    }
                }
                catch (Exception)
                {
                    result = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "UploadFiles");
                }
                return result;
            }
        }

        /// <summary>
        /// 文件服务站点地址
        /// </summary>
        public static string FSWebUrl
        {
            get
            {
                string result = null;
                try
                {
                    object obj = System.Configuration.ConfigurationManager.AppSettings["FSWebUrl"];
                    if (obj != null)
                    {
                        result = obj.ToString();
                    }
                    if (string.IsNullOrEmpty(result))
                    {
                        string curUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                        string temp = HttpContext.Current.Request.Url.AbsoluteUri.Replace("http://", "");
                        string rootUrl = "";
                        int idx = temp.IndexOf("/");
                        if (idx > -1)
                        {
                            rootUrl = temp.Substring(0, temp.IndexOf("/"));
                        }
                        else
                        {
                            rootUrl = temp;
                        }
                        result = "http://" + rootUrl;
                    }
                }
                catch (Exception)
                {
                    result = "";
                }
                return result;
            }
        }

        private static bool _IsUseWCF = true;
        public static bool IsUseWCF
        {
            get
            {
                object obj = System.Configuration.ConfigurationManager.AppSettings["IsUseWCF"];
                if (obj != null)
                {
                    bool.TryParse(obj.ToString(), out _IsUseWCF);
                }

                return _IsUseWCF;
            }
        }

        private static string _SockIOUrl = "127.0.0.1:3000";
        public static string SockIOUrl
        {
            get
            {
                object obj = System.Configuration.ConfigurationManager.AppSettings["SockIOUrl"];
                if (obj != null)
                {
                    _SockIOUrl = obj.ToString();
                }
                return _SockIOUrl;
            }
        }

        private static string _MasterSrvUrl = "";
        public static string MasterSrvUrl
        {
            get
            {
                object obj = System.Configuration.ConfigurationManager.AppSettings["MasterSrvUrl"];
                if (obj != null)
                {
                    _MasterSrvUrl = obj.ToString();
                }
                return _MasterSrvUrl;
            }
        }
    }
}
