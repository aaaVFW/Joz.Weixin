﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Net
{
    public class PathHelper
    {
        public static string GetAppBaseDirectory()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }

        public static string GetUploadPath()
        {
            string path = Config.FSUploadPath;

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
            return path;
        }

        public static string GetUploadPathDirName()
        {
            string dirName = "";
            string path = GetUploadPath();
            if (!string.IsNullOrEmpty(path))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(path);
                if (dirInfo != null)
                {
                    dirName = dirInfo.Name;
                }
            }
            return dirName;
        }

        public static string GetUploadPathParentPath()
        {
            string result = "";
            string path = GetUploadPath();
            if (!string.IsNullOrEmpty(path))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(path);
                if (dirInfo != null && dirInfo.Parent != null)
                {
                    result = dirInfo.Parent.FullName;
                }
                else
                {
                    result = path;
                }
            }
            return result;
        }

        /// <summary>
        /// 临时文件，创建一小时后，自动删除。
        /// </summary>
        /// <returns></returns>
        public static string GetTempUploadPath()
        {
            string path = System.IO.Path.Combine(GetUploadPath(), "Temp");
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
            else
            {
                List<FileInfo> listFileInfo = GetAllFilesInDirectory(path);
                foreach (FileInfo fi in listFileInfo)
                {

                    if (fi != null && fi.CreationTime.AddHours(1) < DateTime.Now)
                    {
                        try
                        {
                            fi.Delete();
                        }
                        catch (Exception ex)
                        {
                            KillProcesses(fi.Extension);
                        }
                    }
                }

            }
            return path;
        }

        private static void KillProcesses(string fileExt)
        {
            string processName = "";
            if (".xls|.xlsx".Contains(fileExt.ToLower()))
            {
                processName = "Excel";
            }
            else if (".doc|.docx".Contains(fileExt.ToLower()))
            {
                processName = "WinWord";
            }

            if (!string.IsNullOrEmpty(processName))
            {
                foreach (Process process in Process.GetProcessesByName(processName))
                {
                    process.Kill();
                }
            }
        }

        /// <summary>
        /// 返回指定目录下的所有文件信息
        /// </summary>
        /// <param name="strDirectory"></param>
        /// <returns></returns>
        public static List<FileInfo> GetAllFilesInDirectory(string strDirectory)
        {
            List<FileInfo> listFiles = new List<FileInfo>(); //保存所有的文件信息  
            DirectoryInfo directory = new DirectoryInfo(strDirectory);
            DirectoryInfo[] directoryArray = directory.GetDirectories();
            FileInfo[] fileInfoArray = directory.GetFiles();
            if (fileInfoArray.Length > 0) listFiles.AddRange(fileInfoArray);
            foreach (DirectoryInfo _directoryInfo in directoryArray)
            {
                DirectoryInfo directoryA = new DirectoryInfo(_directoryInfo.FullName);
                DirectoryInfo[] directoryArrayA = directoryA.GetDirectories();
                listFiles.AddRange(GetAllFilesInDirectory(_directoryInfo.FullName));//递归遍历  
            }
            return listFiles;
        }

        /// <summary>
        /// 返回指定目录下的所有目录信息
        /// </summary>
        /// <param name="strDirectory"></param>
        /// <returns></returns>
        public static List<DirectoryInfo> GetAllDirectorysInDirectory(string strDirectory)
        {
            List<DirectoryInfo> listDirectorys = new List<DirectoryInfo>(); //保存所有的文件信息  
            DirectoryInfo directory = new DirectoryInfo(strDirectory);
            if (directory.Exists)
            {
                DirectoryInfo[] directoryArray = directory.GetDirectories();
                if (directoryArray.Length > 0) listDirectorys.AddRange(directoryArray);
                foreach (DirectoryInfo _directoryInfo in directoryArray)
                {
                    listDirectorys.AddRange(GetAllDirectorysInDirectory(_directoryInfo.FullName));//递归遍历  
                }
            }
            return listDirectorys;
        }

        public static string GetCorpUploadPath(string corpID)
        {
            string path = System.IO.Path.Combine(GetUploadPath(), corpID);
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
            return path;
        }

        public static string GetUserUploadPath(string corpID, long UserID)
        {
            string path = System.IO.Path.Combine(GetCorpUploadPath(corpID), UserID.ToString());
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
            return path;
        }
    }
}
