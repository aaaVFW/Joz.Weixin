﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Common.WeiXin
{
    /// <summary>
    /// 发送语音消息
    /// </summary>
    public class SendVoiceMessage : Message
    {
        public SendVoiceMessage() { actionType = ActionType.Send; }

        public SendVoiceMessage(string xmlMsg)
        {
            actionType = ActionType.Send;
            Parse(xmlMsg);
        }

        protected override bool Parse(string xmlMsg)
        {
            throw new Exception("暂不支持");
        }

        public override string ToString()
        {
            return string.Format("<xml>" + Environment.NewLine +
                "<ToUserName><![CDATA[{0}]]></ToUserName>" + Environment.NewLine +
                "<FromUserName><![CDATA[{1}]]></FromUserName>" + Environment.NewLine +
                "<CreateTime>{2}</CreateTime>" + Environment.NewLine +
                "<MsgType><![CDATA[{3}]]></MsgType>" + Environment.NewLine +
                "<Voice>" + Environment.NewLine +
                "<MediaId><![CDATA[{4}]]></MediaId>" + Environment.NewLine +
                "</Voice>" + Environment.NewLine +
                "</xml>", toUserName, fromUserName, createTime, msgType, mediaId);
        }

        /// <summary>
        /// 语音消息媒体id，可以调用多媒体文件下载接口拉取数据。
        /// </summary>
        public string mediaId { get; set; }
    }
}
