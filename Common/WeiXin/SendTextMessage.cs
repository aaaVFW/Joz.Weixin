﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Common.WeiXin
{
    /// <summary>
    /// 发送文本消息
    /// </summary>
    public class SendTextMessage : Message
    {
        public SendTextMessage() { actionType = ActionType.Send; }

        public SendTextMessage(string xmlMsg)
        {
            actionType = ActionType.Send;
            Parse(xmlMsg);
        }

        protected override bool Parse(string xmlMsg)
        {
            throw new Exception("暂不支持");
        }

        public override string ToString()
        {
            return string.Format("<xml>" + Environment.NewLine +
                "<ToUserName><![CDATA[{0}]]></ToUserName>" + Environment.NewLine +
                "<FromUserName><![CDATA[{1}]]></FromUserName>" + Environment.NewLine +
                "<CreateTime>{2}</CreateTime>" + Environment.NewLine +
                "<MsgType><![CDATA[{3}]]></MsgType>" + Environment.NewLine +
                "<Content><![CDATA[{4}]]></Content>" + Environment.NewLine +
                "</xml>", toUserName, fromUserName, createTime, msgType, content);
        }

        /// <summary>
        /// 文本消息内容
        /// </summary>
        public string content { get; set; }
    }
}
