﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Common.WeiXin
{
    /// <summary>
    /// 接收视频消息
    /// </summary>
    public class ReceiveVideoMessage : Message
    {
        public ReceiveVideoMessage() { actionType = ActionType.Receive; }

        public ReceiveVideoMessage(string xmlMsg)
        {
            actionType = ActionType.Receive;
            Parse(xmlMsg);
        }

        protected override bool Parse(string xmlMsg)
        {
            if (string.IsNullOrEmpty(xmlMsg)) { return false; }
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(xmlMsg);
                XmlNode firstNode = xmlDocument.FirstChild;
                if (firstNode != null)
                {
                    #region 消息类型
                    XmlNode tempNode = firstNode.SelectSingleNode("MsgType");
                    if (tempNode == null) { return false; }
                    msgType = tempNode.InnerText;
                    #endregion
                    #region 发送者
                    tempNode = firstNode.SelectSingleNode("FromUserName");
                    if (tempNode == null) { return false; }
                    fromUserName = tempNode.InnerText;
                    #endregion
                    #region 接收者
                    tempNode = firstNode.SelectSingleNode("ToUserName");
                    if (tempNode == null) return false;
                    toUserName = tempNode.InnerText;
                    #endregion
                    #region 创建时间
                    tempNode = firstNode.SelectSingleNode("CreateTime");
                    if (tempNode == null) return false;
                    createTime = Convert.ToInt64(tempNode.InnerText);
                    #endregion
                    #region 语音消息媒体id
                    tempNode = firstNode.SelectSingleNode("MediaId");
                    if (tempNode == null) return false;
                    mediaId = tempNode.InnerText;
                    #endregion
                    #region 视频缩略图 id
                    tempNode = firstNode.SelectSingleNode("ThumbMediaId");
                    if (tempNode == null) return false;
                    thumbMediaId = tempNode.InnerText;
                    #endregion
                    #region 消息ID
                    tempNode = firstNode.SelectSingleNode("MsgId");
                    if (tempNode == null) return false;
                    msgId = tempNode.InnerText;
                    #endregion
                    return true;
                }
                return false;
            }
            catch { return false; }
        }

        public override string ToString()
        {
            return string.Format("<xml>" + Environment.NewLine +
                "<ToUserName><![CDATA[{0}]]></ToUserName>" + Environment.NewLine +
                "<FromUserName><![CDATA[{1}]]></FromUserName>" + Environment.NewLine +
                "<CreateTime>{2}</CreateTime>" + Environment.NewLine +
                "<MsgType><![CDATA[{3}]]></MsgType>" + Environment.NewLine +
                "<MediaId><![CDATA[{4}]]></MediaId>" + Environment.NewLine +
                "<ThumbMediaId><![CDATA[{5}]]></ThumbMediaId>" + Environment.NewLine +
                "<MsgId>{6}</MsgId>" + Environment.NewLine +
                "</xml>", toUserName, fromUserName, createTime, msgType, mediaId, thumbMediaId, msgId);
        }

        /// <summary>
        /// 语音消息媒体id，可以调用多媒体文件下载接口拉取数据。
        /// </summary>
        public string mediaId { get; set; }

        /// <summary>
        /// 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
        /// </summary>
        public string thumbMediaId { get; set; }

        /// <summary>
        /// 消息id，64位整型
        /// </summary>
        public string msgId { get; set; }
    }
}
