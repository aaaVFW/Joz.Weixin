﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace Common.WeiXin
{
    public abstract class Message
    {
        /// <summary>
        /// 解析XML
        /// </summary>
        /// <param name="xmlMsg">XML字符串</param>
        /// <returns></returns>
        protected abstract bool Parse(string xmlMsg);

        /// <summary>
        /// 生成XML
        /// </summary>
        /// <returns></returns>
        public abstract override string ToString();

        #region 参数
        /// <summary>
        /// 接收人
        /// </summary>
        public string toUserName { get; set; }

        /// <summary>
        /// 发送人
        /// </summary>
        public string fromUserName { get; set; }

        /// <summary>
        /// 消息创建时间
        /// </summary>
        public long createTime { get; set; }

        /// <summary>
        /// 消息类型
        /// </summary>
        public string msgType { get; set; }

        /// <summary>
        /// 响应类型
        /// </summary>
        public ActionType actionType { get; set; }
        #endregion
    }
}
