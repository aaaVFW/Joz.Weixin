﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Common.WeiXin
{
    /// <summary>
    /// 发送图文消息
    /// </summary>
    public class SendArticleMessage : Message
    {
        public SendArticleMessage() { actionType = ActionType.Send; }

        public SendArticleMessage(string xmlMsg)
        {
            actionType = ActionType.Send;
            Parse(xmlMsg);
        }

        protected override bool Parse(string xmlMsg)
        {
            throw new Exception("暂不支持");
        }

        public override string ToString()
        {
            if (ArticleList == null || ArticleList.Count == 0) { throw new Exception("图文列表不能为空"); }
            string msg = "<xml>" + Environment.NewLine +
                "<ToUserName><![CDATA[{0}]]></ToUserName>" + Environment.NewLine +
                "<FromUserName><![CDATA[{1}]]></FromUserName>" + Environment.NewLine +
                "<CreateTime>{2}</CreateTime>" + Environment.NewLine +
                "<MsgType><![CDATA[{3}]]></MsgType>" + Environment.NewLine +
                "<ArticleCount>{4}</ArticleCount>" + Environment.NewLine +
                "<Articles>";
            foreach (Article article in ArticleList) 
            {
                msg += string.Format("<item>" + Environment.NewLine +
                    "<Title><![CDATA[{0}]]></Title>" + Environment.NewLine +
                    "<Description><![CDATA[{1}]]></Description>" + Environment.NewLine +
                    "<PicUrl><![CDATA[{2}]]></PicUrl>" + Environment.NewLine +
                    "<Url><![CDATA[{3}]]></Url>" + Environment.NewLine +
                    "</item>", article.title, article.description, article.picUrl, article.url);
            }
            msg += "</Articles>" + Environment.NewLine + "</xml>";
            return string.Format(msg, toUserName, fromUserName, createTime, msgType, ArticleList.Count);
        }

        /// <summary>
        /// 图文列表
        /// </summary>
        public List<Article> ArticleList { get; set; }
    }
}
