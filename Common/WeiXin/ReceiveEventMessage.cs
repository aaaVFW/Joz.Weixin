﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Common.WeiXin
{
    /// <summary>
    /// 接收事件消息
    /// </summary>
    public class ReceiveEventMessage : Message
    {
        public ReceiveEventMessage() { actionType = ActionType.Receive; }

        public ReceiveEventMessage(string xmlMsg)
        {
            actionType = ActionType.Receive;
            Parse(xmlMsg);
        }

        protected override bool Parse(string xmlMsg)
        {
            if (string.IsNullOrEmpty(xmlMsg)) { return false; }
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(xmlMsg);
                XmlNode firstNode = xmlDocument.FirstChild;
                if (firstNode != null)
                {
                    #region 消息类型
                    XmlNode tempNode = firstNode.SelectSingleNode("MsgType");
                    if (tempNode == null) { return false; }
                    msgType = tempNode.InnerText;
                    #endregion
                    #region 发送者
                    tempNode = firstNode.SelectSingleNode("FromUserName");
                    if (tempNode == null) { return false; }
                    fromUserName = tempNode.InnerText;
                    #endregion
                    #region 接收者
                    tempNode = firstNode.SelectSingleNode("ToUserName");
                    if (tempNode == null) return false;
                    toUserName = tempNode.InnerText;
                    #endregion
                    #region 创建时间
                    tempNode = firstNode.SelectSingleNode("CreateTime");
                    if (tempNode == null) return false;
                    createTime = Convert.ToInt64(tempNode.InnerText);
                    #endregion
                    #region 事件
                    tempNode = firstNode.SelectSingleNode("Event");
                    if (tempNode == null) { return false; }
                    eventType = tempNode.InnerText;
                    #endregion
                    #region 事件KEY值
                    tempNode = firstNode.SelectSingleNode("EventKey");
                    if (tempNode != null) { eventKey = tempNode.InnerText; }
                    #endregion
                    return true;
                }
                return false;
            }
            catch { return false; }
        }

        public override string ToString()
        {
            return string.Format("<xml>" + Environment.NewLine +
                "<ToUserName><![CDATA[{0}]]></ToUserName>" + Environment.NewLine +
                "<FromUserName><![CDATA[{1}]]></FromUserName>" + Environment.NewLine +
                "<CreateTime>{2}</CreateTime>" + Environment.NewLine +
                "<MsgType><![CDATA[{3}]]></MsgType>" + Environment.NewLine +
                "<Event><![CDATA[{4}]]></Event>" + Environment.NewLine +
                "<EventKey><![CDATA[{5}]]></EventKey>" + Environment.NewLine +
                "</xml>", toUserName, fromUserName, createTime, msgType, eventType, eventKey);
        }

        /// <summary>
        /// 事件类型，subscribe/click/view
        /// </summary>
        public string eventType { get; set; }

        /// <summary>
        /// 事件KEY值，与自定义菜单接口中KEY值/跳转URL对应
        /// </summary>
        public string eventKey { get; set; }
    }
}
