﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.WeiXin
{
    public class CustomMenu
    {

    }
   
    /// button :一级菜单数组，个数应为1~3个
    /// <summary>
    /// 发送菜单JSON类
    /// </summary>
    public class MenuParameter
    {
        /// <summary>
        /// 必须：否
        /// 二级菜单数组，个数应为1~5个
        /// </summary>
        public List<MenuParameter> sub_button { get; set; }
        /// <summary>
        /// 必须：是
        /// 菜单的响应动作类型
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// 必须：是
        /// 菜单标题，不超过16个字节，子菜单不超过40个字节
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 类型为“click”等点击类型必须
        /// 菜单KEY值，用于消息接口推送，不超过128字节
        /// </summary>
        public string key { get; set; }
        /// <summary>
        /// 类型为“view” 必须	
        /// 网页链接，用户点击菜单可打开链接，不超过1024字节
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// media_id类型和view_limited类型必须
        /// 调用新增永久素材接口返回的合法media_id
        /// </summary>
        public string media_id { get; set; }
    }

    /// <summary>
    /// 自定义菜单类型
    /// 除了click,view,所有事件，仅支持微信iPhone5.4.1以上版本，和Android5.4以上版本的微信用户
    /// </summary>
    public enum CustomType
    {
        /// <summary>
        /// 点击按钮事件，返回key给开发者处理
        /// </summary>
        click,
        /// <summary>
        /// 点击按钮根据配置URL跳转
        /// </summary>
        view,
        /// <summary>
        ///点击按钮，弹出扫码，返回信息处理， 扫码推事件
        /// </summary>
        scancode_push,
        /// <summary>
        /// 扫码推事件且弹出“消息接收中”提示框
        /// </summary>
        scancode_waitmsg,
        /// <summary>
        /// 弹出系统拍照发图
        /// </summary>
        pic_sysphoto,
        /// <summary>
        /// 弹出拍照或者相册发图
        /// </summary>
        pic_photo_or_album,
        /// <summary>
        /// 弹出微信相册发图器
        /// </summary>
        pic_weixin,
        /// <summary>
        /// 弹出地理位置选择器
        /// </summary>
        location_select,
        /// <summary>
        /// 下发消息（除文本消息）
        /// </summary>
        media_id,
        /// <summary>
        /// 跳转图文消息URL
        /// </summary>
        view_limited
    }
}
