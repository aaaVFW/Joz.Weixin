﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.WeiXin
{
    public class MessageType
    {
        /// <summary>
        /// 文本
        /// </summary>
        public static readonly string Text = "text";
        /// <summary>
        /// 图片
        /// </summary>
        public static readonly string Image = "image";
        /// <summary>
        /// 地理位置
        /// </summary>
        public static readonly string Location = "location";
        /// <summary>
        /// 链接
        /// </summary>
        public static readonly string Link = "link";
        /// <summary>
        /// 事件
        /// </summary>
        public static readonly string Event = "event";
        /// <summary>
        /// 语音
        /// </summary>
        public static readonly string Music = "voice";
        /// <summary>
        /// 视频
        /// </summary>
        public static readonly string Video = "video";
        /// <summary>
        /// 图文
        /// </summary>
        public static readonly string News = "news";
    }

    /// <summary>
    /// 响应类型
    /// </summary>
    public enum ActionType
    {
        Receive,
        Send
    }
}
