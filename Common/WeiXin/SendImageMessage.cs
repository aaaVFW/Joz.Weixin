﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Common.WeiXin
{
    /// <summary>
    /// 发送图片消息
    /// </summary>
    public class SendImageMessage : Message
    {
        public SendImageMessage() { actionType = ActionType.Send; }

        public SendImageMessage(string xmlMsg)
        {
            actionType = ActionType.Send;
            Parse(xmlMsg);
        }

        protected override bool Parse(string xmlMsg)
        {
            throw new Exception("暂不支持");
        }

        public override string ToString()
        {
            return string.Format("<xml>" + Environment.NewLine +
                "<ToUserName><![CDATA[{0}]]></ToUserName>" + Environment.NewLine +
                "<FromUserName><![CDATA[{1}]]></FromUserName>" + Environment.NewLine +
                "<CreateTime>{2}</CreateTime>" + Environment.NewLine +
                "<MsgType><![CDATA[{3}]]></MsgType>" + Environment.NewLine +
                "<Image>" + Environment.NewLine +
                "<MediaId><![CDATA[{4}]]></MediaId>" + Environment.NewLine +
                "</Image>" + Environment.NewLine +
                "</xml>", toUserName, fromUserName, createTime, msgType, mediaId);
        }

        /// <summary>
        /// 图片消息媒体id，可以调用多媒体文件下载接口拉取数据
        /// </summary>
        public string mediaId { get; set; }
    }
}
