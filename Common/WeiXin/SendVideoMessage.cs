﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Common.WeiXin
{
    /// <summary>
    /// 发送视频消息
    /// </summary>
    public class SendVideoMessage : Message
    {
        public SendVideoMessage() { actionType = ActionType.Send; }

        public SendVideoMessage(string xmlMsg)
        {
            actionType = ActionType.Send;
            Parse(xmlMsg);
        }

        protected override bool Parse(string xmlMsg)
        {
            throw new Exception("暂不支持");
        }

        public override string ToString()
        {
            return string.Format("<xml>" + Environment.NewLine +
                "<ToUserName><![CDATA[{0}]]></ToUserName>" + Environment.NewLine +
                "<FromUserName><![CDATA[{1}]]></FromUserName>" + Environment.NewLine +
                "<CreateTime>{2}</CreateTime>" + Environment.NewLine +
                "<MsgType><![CDATA[{3}]]></MsgType>" + Environment.NewLine +
                "<Video>" + Environment.NewLine +
                "<MediaId><![CDATA[{4}]]></MediaId>" + Environment.NewLine +
                "<Title><![CDATA[{5}]]></Title>" + Environment.NewLine +
                "<Description><![CDATA[{6}]]></Description>" + Environment.NewLine +
                "</Video>" + Environment.NewLine +
                "</xml>", toUserName, fromUserName, createTime, msgType, mediaId, title, description);
        }

        /// <summary>
        /// 语音消息媒体id，可以调用多媒体文件下载接口拉取数据。
        /// </summary>
        public string mediaId { get; set; }

        /// <summary>
        /// 视频消息的标题
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 视频消息的描述
        /// </summary>
        public string description { get; set; }
    }
}
