﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.WeiXin
{
    /// <summary>
    /// 图文消息
    /// </summary>
    public class Article
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// 图片链接，支持JPG、PNG格式，较好的效果为大图360*200，小图200*200
        /// </summary>
        public string picUrl { get; set; }

        /// <summary>
        /// 点击图文消息跳转链接
        /// </summary>
        public string url { get; set; }
    }
}
