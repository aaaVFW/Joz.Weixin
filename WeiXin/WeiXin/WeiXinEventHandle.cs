﻿using Common.WeiXin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using System.Globalization;

namespace MobileWeb.App_Code
{
    public class WeiXinEventHandle
    {
        public WeiXinEventHandle(HttpContext context, string xml)
        {
            this.context = context;
            this.message = new ReceiveEventMessage(xml);
            Process();
        }
        public void Process()
        {
            switch (message.eventType.ToLower())
            {
                case "subscribe":
                    Subscribe();
                    break;
                case "unsubscribe":
                    UnSubscribe();
                    break;
                case "click":
                    Click();
                    break;
            }
        }
        /// <summary>
        /// 关注微信号之后，发送的信息模板
        /// </summary>
        void Subscribe()
        {
            SendTextMessage text = new SendTextMessage()
            {
                fromUserName = WeiXinHelper.weiXinNum,
                toUserName = message.fromUserName,
                createTime = Convert.ToInt64(DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture)),
                msgType = "text",
                actionType = ActionType.Send,
                content = "感谢关注【资源这里有】微信号\n\n"
                          + "我们已经积累了超过5万的资源库，有着丰富的影视剧资源"
                          + "\n\n 将提供以下服务："
                          + "\n 1.回复【主演或影视剧名称】来搜索查询"
                          + "\n 2.<a href =\"{0}\">定期更新</a>最新电影，电视剧，综艺等类型资源"
                          + "\n 3.为公众用户提供<a href =\"{1}\"> 求种/留言 </a>"
            };
            text.content = string.Format(text.content
                , ConfigurationManager.AppSettings["WeiXinReturnHost"] + "/index.html"//按影视剧名
                , ConfigurationManager.AppSettings["WeiXinReturnHost"] + "/index.html");//求种/留言
            context.Response.Write(text.ToString());
        }
        /// <summary>
        /// 取消关注之后，获取的相关信息
        /// </summary>
        void UnSubscribe()
        {
            //LS_Users_OpenID_Bll.Instance.Remove(message.fromUserName, EnumOpenIdFromWeb.微信);
        }

        void Click()
        {
            switch (message.eventKey)
            {
                case "advice":
                    SendTextMessage text = new SendTextMessage()
                    {
                        fromUserName = WeiXinHelper.weiXinNum,
                        toUserName = message.fromUserName,
                        createTime = Convert.ToInt64(DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture)),
                        msgType = "text",
                        actionType = ActionType.Send,
                        content = "若在使用律师帮过程出现问题或想给我们更好的建议,欢迎您直接回复消息给我们,非常感谢！"
                    };
                    context.Response.Write(text.ToString());
                    break;
                case "A_News_dianying":
                    List<Article> artList = new List<Article>() {
                        new Article()
                        {
                            description = "这部电影不错啊这部电影不错啊这部电影不错啊这部电影不错啊",
                            picUrl = "http://zhongjie1974.6655.la/images/1234.jpg",
                            title = "很不错的一步电影",
                            url = "http://zhongjie1974.6655.la/library.aspx"
                        },
                         new Article()
                        {
                            description = "这部电影不错啊这部电影不错啊这部电影不错啊这部电影不错啊",
                            picUrl = "http://zhongjie1974.6655.la/images/1234.jpg",
                            title = "第二部",
                            url = "http://zhongjie1974.6655.la/library.aspx"
                        },
                            new Article()
                        {
                            description = "这部电影不错啊这部电影不错啊这部电影不错啊这部电影不错啊",
                            picUrl = "http://zhongjie1974.6655.la/images/1234.jpg",
                            title = "第三部",
                            url = "http://zhongjie1974.6655.la/library.aspx"
                        },
                               new Article()
                        {
                            description = "这部电影不错啊这部电影不错啊这部电影不错啊这部电影不错啊",
                            picUrl = "http://zhongjie1974.6655.la/images/1234.jpg",
                            title = "第四部",
                            url = "http://zhongjie1974.6655.la/library.aspx"
                        }
                    };

                    SendArticleMessage msg = new SendArticleMessage()
                    {
                        actionType = ActionType.Send,
                        createTime = Convert.ToInt64(DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture)),
                        fromUserName = WeiXinHelper.weiXinNum,
                        toUserName = message.fromUserName,
                        msgType = MessageType.News,
                        ArticleList = artList
                    };
                    context.Response.Write(msg.ToString());
                    break;
                case "A_News_dianshiju":
                    List<Article> News_dianshiju = new List<Article>() {
                        new Article()
                        {
                            description = "这是电视剧这是电视剧这是电视剧这是电视剧这是电视剧",
                            picUrl = "http://zhongjie1974.6655.la/images/1234.jpg",
                            title = "这是电视剧",
                            url = "http://zhongjie1974.6655.la/library.aspx"
                        },
                         new Article()
                        {
                            description = "这是电视剧这是电视剧这是电视剧这是电视剧这是电视剧",
                            picUrl = "http://zhongjie1974.6655.la/images/1234.jpg",
                            title = "这是电视剧这是电视剧这是电视剧这是电视剧",
                            url = "http://zhongjie1974.6655.la/library.aspx"
                        }
                    };

                    SendArticleMessage msgNews_dianshiju = new SendArticleMessage()
                    {
                        actionType = ActionType.Send,
                        createTime = Convert.ToInt64(DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture)),
                        fromUserName = WeiXinHelper.weiXinNum,
                        toUserName = message.fromUserName,
                        msgType = MessageType.News,
                        ArticleList = News_dianshiju
                    };
                    context.Response.Write(msgNews_dianshiju.ToString());
                    break;
                case "A_News_zongyi":
                    List<Article> artdianshiList = new List<Article>() {
                        new Article()
                        {
                            description = "这是一部综艺这是一部综艺这是一部综艺这是一部综艺这是一部综艺这是一部综艺这是一部综艺",
                            picUrl = "http://zhongjie1974.6655.la/images/1234.jpg",
                            title = "这是一部综艺这是一部综艺",
                            url = "http://zhongjie1974.6655.la/library.aspx"
                        }
                    };

                    SendArticleMessage msgdianshi = new SendArticleMessage()
                    {
                        actionType = ActionType.Send,
                        createTime = Convert.ToInt64(DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture)),
                        fromUserName = WeiXinHelper.weiXinNum,
                        toUserName = message.fromUserName,
                        msgType = MessageType.News,
                        ArticleList = artdianshiList
                    };
                    context.Response.Write(msgdianshi.ToString());
                    break;
            }

        }

        /// <summary>
        /// 当前上下文
        /// </summary>
        HttpContext context;

        /// <summary>
        /// 是否写调试日志
        /// </summary>
        static readonly bool debugLog = ConfigurationManager.AppSettings["WeiXinDebugEnable"] == "1";

        /// <summary>
        /// 消息对象
        /// </summary>
        ReceiveEventMessage message { get; set; }
    }
}