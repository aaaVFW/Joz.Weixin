﻿using Common.WeiXin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using Common.Net;

namespace MobileWeb.App_Code
{
    public class WeiXinTextHandle
    {
        public WeiXinTextHandle(HttpContext context, string xml)
        {
            this.context = context;
            message = new ReceiveTextMessage(xml);
            Process();
        }

        public void Process()
        {
            SendTextMessage msg = new SendTextMessage
            {
                fromUserName = WeiXinHelper.weiXinNum,
                toUserName = message.fromUserName,
                createTime = Convert.ToInt64(DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture)),
                actionType = ActionType.Send,
                msgType = "text",
                content = "感谢您的宝贵意见，祝您工作顺利！"
            };
            context.Response.Write(msg.ToString());
            return;
        }

        private void Write(string str)
        {
            context.Response.Clear();
            context.Response.Write(str);
            context.Response.End();
        }

        /// <summary>
        /// 当前上下文
        /// </summary>
        HttpContext context;

        /// <summary>
        /// 是否写调试日志
        /// </summary>
        static readonly bool debugLog = ConfigurationManager.AppSettings["WeiXinDebugEnable"] == "1";

        /// <summary>
        /// 消息对象
        /// </summary>
        ReceiveTextMessage message { get; set; }
    }
}