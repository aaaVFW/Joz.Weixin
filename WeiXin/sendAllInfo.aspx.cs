﻿using Common.WeiXin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Common.Net;
using LitJson;
using Common;
namespace WeiXin
{
    public partial class sendAllInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //SendOpenIDUser();
            sendUserModel("qSUDHvL2LS6MC1EU05Ck8sy4bny1qOAKw1ahK5w9Opc");
        }

        //根据模板ID和openID发送到指定用户
        public void sendUserModel(string template_id)
        {
            //sendUserModel("zETvTek0lfc-ZiplOC7nUUbkfczHzrnKfJILnuutRUY");
            sendUserModel("qSUDHvL2LS6MC1EU05Ck8sy4bny1qOAKw1ahK5w9Opc");
            var json = new
            {
                touser = "ohwcwv52lYWtjx6cFxdFLEHxSvO8",
                template_id = template_id,

                //以下的data参数  是根据模板的字符替换，类似string.format
                //data = new
                //{
                //    first = new { value = title },
                //    keyword1 = new { value = subject },
                //    keyword2 = new { value = content },
                //    keyword3 = new { value = date.ToString("MM月dd日 HH:mm:ss") },
                //    remark = new { value = remark }
                //},
                //点击这个模板消息的路径
                //url = url
            };
            string msg = JsonMapper.ToJson(json);
            string result = HttpRequestHelper.postRequest(WeiXinHelper.templateMsgUri, msg);
            //  { "errcode":0,"errmsg":"ok", "msgid":200228332}
            //正常返回结果，可记录msg_id以便查询记录
        }

        /// <summary>
        /// 无差别群发
        /// </summary>
        public void SendAllUser()
        {
            string json = "{" +
              "\"filter\":{" +
                "\"is_to_all\":true" +
            "}," +
               "\"text\":{" +
                    "\"content\":\"无差别群发\"" +
              " }," +
                "\"msgtype\":\"text\"" +
            "}";
            string sendUrl = string.Format("https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token={0}", WeiXinHelper.accessToken);
            string result = HttpRequestHelper.postRequest(sendUrl, json);
            //{"errcode":0,   "errmsg":"send job submission success",   "msg_id":34182,    "msg_data_id": 206227730}
            //正常返回结果，可记录msg_id以便查询记录
        }
        /// <summary>
        /// 根据OPENID组合，最少两个，群发
        /// </summary>
        /// <param name="OpenIDs"></param>
        public void SendOpenIDUser()
        {
            string json = "{" +
               "\"touser\":[" +
               "\"ohwcwv3c_uj80jaqCwbV1LPTIfZQ\"," +
                "\"ohwcwv52lYWtjx6cFxdFLEHxSvO8\"," +
                "\"ohwcwvxfO17HdbOz6i5rUFLRp_eI\"" +
               "]," +
              "  \"msgtype\": \"text\"," +
               " \"text\": { \"content\": \"hello from boxer.\"}" +
            "}";
            string strurl = string.Format("https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token={0}", WeiXinHelper.accessToken);
            string result = HttpRequestHelper.postRequest(strurl, json);

            //{"errcode":0,"errmsg":"send job submission success","msg_id":3147483651}
            //正常返回结果，可记录msg_id以便查询记录
        }
    }
}