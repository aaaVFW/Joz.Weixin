﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.WeiXin;
using Common.Net;
using Newtonsoft.Json;
using System.Text;

namespace WeiXin
{
    public partial class testCustomMenu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CustomJsonData();
            string result = Menu("lvshibang", "admin");
        }
        public void CustomJsonData()
        {
            string json = "{\"button\":[" +
                         "{\"name\":\"资源推荐\"," +
                             "\"sub_button\":[" +
                              "{\"type\":\"click\"," +
                              "\"name\":\"最新电影\"," +
                              "\"key\":\"A_News_dianying\"" +
                              "}," +
                              "{" +
                              "\"type\":\"click\"," +
                              "\"name\":\"最新电视剧\"," +
                              "\"key\":\"A_News_dianshiju\"" +
                               "}," +
                               "{" +
                              "\"type\":\"click\"," +
                              "\"name\":\"最新综艺\"," +
                              "\"key\":\"A_News_zongyi\"" +
                               "}]" +
                         "}," +
                         "{\"name\":\"资源搜索\"," +
                             "\"sub_button\":[" +
                              "{" +
                              "\"type\":\"view\"," +
                              "\"name\":\"影视库\"," +
                              "\"url\":\"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxb6ec554f0693f6d7&redirect_uri=http%3a%2f%2fzhongjie1974.6655.la%2factionform.aspx%3freurl%3d11&response_type=code&scope=snsapi_base#wechat_redirect\"" +
                               "}," +
                             "{" +
                              "\"type\":\"view\"," +
                              "\"name\":\"最近更新\"," +
                                 "\"url\":\"http://zhongjie1974.6655.la/news.aspx\"" +
                             " }]" +
                         "}," +
                         "{\"name\":\"更多功能\"," +
                             "\"sub_button\":[" +
                              "{" +
                              "\"type\":\"view\"," +
                              "\"name\":\"订阅影视资源\"," +
                              "\"url\":\"http://zhongjie1974.6655.la/subscribe.aspx\"" +
                               "}," +
                             "{" +
                              "\"type\":\"view\"," +
                              "\"name\":\"求种留言\"," +
                              "\"url\":\"http://zhongjie1974.6655.la/leav.aspx\"" +
                             " }]" +
                         "}]" +
                     "}";

            string result = MenuSet("lvshibang", "admin", json);


        }
        #region 微信自定义菜单
        public string Menu(string user, string pwd)
        {
            if (user != "lvshibang" || pwd != "admin") return "参数错误";
            string url = string.Format("https://api.weixin.qq.com/cgi-bin/menu/get?access_token={0}", WeiXinHelper.accessToken);
            string result = HttpRequestHelper.getContentUTF8(url);

            return result;
        }

        public string MenuSet(string user, string pwd, string json)
        {
            if (user != "lvshibang" || pwd != "admin") return "参数错误";
            string url = string.Format("https://api.weixin.qq.com/cgi-bin/menu/create?access_token={0}", WeiXinHelper.accessToken); //
            string result = HttpRequestHelper.postRequest(url, json);
            return result;
        }
        #endregion
    }
}