﻿using Common.Net;
using Common.WeiXin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WeiXin
{
    public partial class testCustomerService : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //AddCustomer();
        }

        public string AllCustomer()
        {
            string url = string.Format("https://api.weixin.qq.com/cgi-bin/customservice/getkflist?access_token={0}", WeiXinHelper.accessToken);
            string result = HttpRequestHelper.getContentUTF8(url);
            return result;
        }

        public string AddCustomer()
        {
            string json = "\"kf_account\" : \"2108852774@qq.com\", \"nickname\" : \"客服1\",     \"password\" : \"pswmd5\",";
            string url = string.Format("https://api.weixin.qq.com/customservice/kfaccount/add?access_token={0}", WeiXinHelper.accessToken);
            string result = HttpRequestHelper.postRequest(url, json);
            return result;
        }
        public string EditCustomer()
        {
            string json = "\"kf_account\" : \"test1@test\", \"nickname\" : \"客服1\",     \"password\" : \"pswmd5\",";
            string url = string.Format("https://api.weixin.qq.com/customservice/kfaccount/update?access_token={0}", WeiXinHelper.accessToken);
            string result = HttpRequestHelper.postRequest(url, json);
            return result;
        }
        public string DelCustomer()
        {
            string json = "\"kf_account\" : \"test1@test\", \"nickname\" : \"客服1\",     \"password\" : \"pswmd5\",";
            string url = string.Format("https://api.weixin.qq.com/customservice/kfaccount/del?access_token={0}", WeiXinHelper.accessToken);
            string result = HttpRequestHelper.getContentUTF8(url, json);
            return result;
        }
        /// <summary>
        /// 上传客服头像，类型：jpg,大小640*640  小于1M
        /// </summary>
        /// <param name="account">客服账号</param>
        /// <param name="fileImg">客服头像file数据流</param>
        /// <returns></returns>
        public string UploadCustomerHeadImg(string account, string fileImg)
        {
            string url = string.Format("http://api.weixin.qq.com/customservice/kfaccount/uploadheadimg?access_token={0}&kf_account={1}", WeiXinHelper.accessToken, account); //
            string result = HttpRequestHelper.postRequest(url, fileImg);
            return result;
        }
        /// <summary>
        /// 发送文本信息
        /// </summary>
        /// <param name="msg">发送内容</param>
        /// <param name="toUserID">接收人openID</param>
        /// <param name="account">客服账号</param>
        /// <returns></returns>
        public string CustomerSendMsg(string content, string toUserID, string account)
        {
            string msg = string.Format("\"touser\":\"{0}\",\"msgtype\":\"text\",\"customservice\":{ \"kf_account\": \"{1}\"},\"text\":{ \"content\":\"{2}\"}", toUserID, account, content);
            /***剩余发送消息种类，查看API说明文档***/
            string url = string.Format("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={0}", WeiXinHelper.accessToken);
            string result = HttpRequestHelper.postRequest(url, msg);
            return result;
        }
        
    }
}